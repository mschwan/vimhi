* Clean up highlighting file by removing duplicate lines
* Add option to pass file with extra words to be highlighted, that do not have a
  ctags reference.
