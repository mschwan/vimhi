vimhi
================================================================================

`vimhi` is a small BASH-script that uses Ctags to generate VIM syntax
highlighting files for your local projects.

Installation
--------------------------------------------------------------------------------

Use the provided Makefile to install `vimhi` to `/usr/local/bin/`:
```sh
make install
```
Or simpy copy `vimhi` to any custom installation path.

For the syntax highlighting to be shown in VIM simply source the generated
syntax file. For example, add this to your `.vimrc` to enable syntax
highlighting for C related source files:
```vim
if filereadable(expand('~/.cache/vimhi/$PWD/vimhi'))
    autocmd FileType c.doxygen,c,cpp source ~/.cache/vimhi/$PWD/vimhi
endif
```

Usage
--------------------------------------------------------------------------------

Execute `vimhi` with the paths provided Ctags should recursively search in:
```sh
vimhi src/  # search inside the projects local src/ directory
vimhi /usr/include/GL/  # search inside the system wide includes for OpenGL
vimhi /usr/include/glib-2.0/ src/  # search inside multiple directories
```

![vimhi example](vimhi-example.png)

License
--------------------------------------------------------------------------------

`vimhi` is licensed under the MIT License (MIT).

    Copyright © 2018 Martin Schwan

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the “Software”), to
    deal in the Software without restriction, including without limitation the
    rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
    sell copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
