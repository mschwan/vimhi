#!/usr/bin/env python3

# vimhi - Create Ctags-based syntax highlighting for Vim
# Copyright (c) 2019  Martin Schwan <mschwan@posteo.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import argparse
from random import choice
from string import ascii_letters, digits
import subprocess
import os

VIMHI_DIR = os.path.expandvars('${HOME}/.cache/vimhi')

TAGS_C = {
        '-functions': 'fp',
        '-defines': 'd',
        '-enumerations': 'e',
        '-structs': 'tgs'
}

HIGHLIGHTING_C = {
        '_functions': 'Function',
        '_defines': 'Define',
        '_enumerations': 'Number',
        '_structs': 'Type'
}

def create_tags_for_kinds(args, name, kinds, extra_tags):
    kinds_argument = '--{}-kinds={}'.format(args.language, kinds)
    command = [args.bin, '-R', kinds_argument, '-f',
            os.path.join(VIMHI_DIR, name), *args.files]
    print(command)

    # make sure the directory ~/.cache/vimhi/ exists
    if not os.path.exists(VIMHI_DIR):
        os.makedirs(VIMHI_DIR)

    # execute ctags to generate tags file
    subprocess.run(command)

    # read tags file and extract first word of each line
    tags = []
    with open(os.path.join(VIMHI_DIR, name), 'r') as f:
        for line in f:
            if line[0] != '!':
                tags.append(line.split(None, 1)[0])
    print(tags)

    if extra_tags is not None:
        extra_tags = extra_tags.split(',')

    # copy words to new syntax file
    syntax_file = os.path.join(VIMHI_DIR + os.getcwd(), args.language)
    syntax_dir = os.path.dirname(syntax_file)
    print(syntax_file, syntax_dir)
    if not os.path.exists(syntax_dir):
        print('Creating directory')
        os.makedirs(syntax_dir)
    with open(syntax_file, 'a') as f:
        # append tags to vim syntax file
        f.write('syn keyword vimhi_{}'.format(name[9:]))
        for tag in tags:
            f.write('\n\t\\ {}'.format(tag))
        if extra_tags is not None:
            for extra_tag in extra_tags:
                f.write('\n\t\\ {}'.format(extra_tag))
        f.write('\n')

    # clean up any unneeded files
    os.remove(os.path.join(VIMHI_DIR, name))

def main(args):
    if args.language != 'c':
        print('Languages other than C are currently not supported!')
        return

    random_name = ''.join([choice(ascii_letters + digits) for i in range(8)])
    if not os.path.exists(VIMHI_DIR + os.getcwd()):
        os.makedirs(VIMHI_DIR + os.getcwd())
    open(os.path.join(VIMHI_DIR + os.getcwd(), args.language), 'w').close()
    extra_tags_dict = {
        '-functions': None,
        '-defines': None,
        '-enumerations': None,
        '-structs': args.extra_types
    }
    for name, kinds in TAGS_C.items():
        print(name, kinds)
        create_tags_for_kinds(args, random_name + name, kinds, extra_tags_dict[name])
    for name, highlighting in HIGHLIGHTING_C.items():
        print(name, highlighting)
        with open(os.path.join(VIMHI_DIR + os.getcwd(), args.language), 'a') as f:
            f.write('hi def link vimhi{} {}\n'.format(name, highlighting))

    # generate regular tags
    kinds = ''.join([kinds for name, kinds in TAGS_C.items()])
    subprocess.run([args.bin, '-R', '--{}-kinds={}'.format(args.language, kinds),
        *args.files])

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Create Ctags-based syntax '
            'highlighting for Vim.',
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-l', '--language', type=str, default='c',
            choices=['c', 'cpp', 'python'],
            help='The language that is being used for creating the tags.')
    parser.add_argument('-b', '--bin', type=str, action='store',
            default='ctags', help='The path to the Ctags binary.')
    parser.add_argument('--extra-types', type=str, action='store',
            help='Extra types to highlight, that do not have any tags')
    parser.add_argument('files', type=str, nargs='+', help='A list of files '
            'to be passed to Ctags.')

    main(parser.parse_args())
